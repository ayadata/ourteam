<x-app-layout>
    <x-slot name="header">
        @include('layouts.navigation')
    </x-slot>

    <!--body content wrap start-->
    <div class="main">

        <!--header section start-->
        <section class="hero-section ptb-100 gradient-overlay"
                 style="background: url('{{ asset('img/header-bg-5.jpg') }}')no-repeat center center / cover">
            <div class="hero-bottom-shape-two"
                 style="background: url('{{ asset('img/hero-bottom-shape.svg') }}')no-repeat bottom center"></div>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-7">
                        <div class="page-header-content text-white text-center pt-sm-5 pt-md-5 pt-lg-0">
                            <h1 class="text-white mb-0">Our Team</h1>
                            <div class="custom-breadcrumb">
                                <!-- <ol class="breadcrumb d-inline-block bg-transparent list-inline py-0">
                                    <li class="list-inline-item breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="list-inline-item breadcrumb-item"><a href="#">Pages</a></li>
                                    <li class="list-inline-item breadcrumb-item active">About Us</li>
                                </ol> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--header section end-->

       
        <!--client section start-->

    </div>
    <!--body content wrap end-->

<!---Leadership starts here -->

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
       

@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&display=swap');

@import 'https://fonts.googleapis.com/css?family=Raleway:100,600';
@import 'https://fonts.googleapis.com/css?family=Open+Sans:300';
@import url("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");

 .section-title h2 {
  text-transform: uppercase;
}
.section-title {
  margin: 3% 0;
}
.section-title p {
  text-transform: uppercase;
  font-weight:bold;
}
.single-team{
  padding-bottom: 70px;
  position: relative;
  z-index: 1;
  overflow: hidden;
  box-shadow:5px 5px 15px grey;
  margin-top:20px;
}

.team-img{
  position: relative;
  z-index: 5;
  overflow: hidden;
}
.team-img img{
  width: 100%;
  transition: .3s;
}
.single-team:hover .team-img img {
  transform: scale(1.1);
  opacity:0.5; /*--Set Opacity here*/
}
.team-content {
  height: 80px;
  width: 100%;
  position: absolute;
  text-align: center;
  overflow: hidden;
  bottom: 0;
  transition: all .4s;
  background: #162e66;
  z-index:5;
}
.single-team:hover .team-content{
  height: 350px;                     
  justify-content:space-evenly; /*--Justify content tag here*/
}
.single-team .team-content:hover .team-text {
position:relative;
top:-35px;
}


.single-team .team-content:hover .title {

color:pink;
position:relative;
top:-10px;


}

.title{
  position:relative;
  top:-4px;
}

.team-info {
  padding: 5px 20px 5px 20px;
  transition: all .5s;
}
.single-team:hover .team-content{       
  background: #162e66;  /*--Set Opacity here*/
  opacity:0.7;
}
.single-team .team-content .team-info h3 {      
  text-transform: uppercase;
  color: #fff;
  font-size: 15px;
  margin: 5px;
}
.single-team:hover .team-info h3{
  color: white;
}
.team-info p {
  margin-bottom: 25px;
  color: #fff;
  text-align:center;
  
  
}


.team-text {
  color: #fff;
  padding: 0 10px 5px;
  font-size:15px;
}

.shift{

    padding-bottom:30px;
}



/* ignore the code below */


.link-area
{
  position:fixed;
  bottom:20px;
  left:20px;  
  padding:15px;
  border-radius:40px;
  background:tomato;
  z-index: 10;
}
.link-area a
{
  text-decoration:none;
  color:#fff;
  font-size:20px;
}
.team-content .team-info .sci {

    display: inline-block;
	/* height: 25px; */
	/* width: 25px; */
	background-color: #ffffff;
	color:#009688;
	border-radius: 50%;
	margin:0 2px;
	text-align: center;
	/* line-height: 32px; */
	/* font-size:20px; */
	transition: all .5s ease;
    float:right; 
    position: relative;
    top:-30px;
    left: 15px;
   
  
   
    
}

.team-content .team-info .sci  a{
padding: 7px;
transition:.5s;

}


 

@media(max-width: 767px){
	.team-area .single-team .team-content{
      flex-basis: calc(100%);
       max-width: calc(100%);

   }
   @media(max-width: 991px){
	.team-area .single-team .item-content{
      flex-basis: calc(50% - 30px);
       max-width: calc(50% - 30px);

   }
}


@media screen and (max-width:70px) {
    
    section-title text-center h1{
        text-align: center;
        font-size: 34px;
        font-family: 'Kaushan Script',
       
    }

}
}





</style>

</head>
<div class="shift">
<body>
 
<div class="team-area">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title text-center">
          <h2>Meet Our  Team Members</h2>
          <p></p>
        </div>
      </div>
       <!--== Single Team Item ==-->
       <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img style="max-width:98%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" title="Freddie"
       src="/img/Ama.png" alt="" class="img-responsive">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Ama <br>Larbi- Siaw</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/ama-ls-671178b2/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;">   Co-Founder</p>                   
            </div>
            <p class="team-text"> 
            Ama co-founded Aya Data after 10 years working in London’s commercial finance sector. She brings experience in developing efficient workflow systems, understanding and implementation of compliance,
            and leadership qualities that reflect the philosophy and mission of Aya. 
          

            </p>
          </div>
        </div>
      </div>

       <!--== Single Team Item ==-->
       <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img style="max-width:98%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" title="Freddie"
       src="/img/fred.png" alt="" class="img-responsive">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Freddie <br> Monk</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/freddie-monk-38347456/?originalSubdomain=uk" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;">   Co-Founder</p>                   
            </div>
            <p class="team-text"> 
            Freddie spent four years in EY’s UK Strategy Practice before transitioning to finance lead at Demeter Holdings investment group.
          After 2 years of living and working in Ghana, Freddie co-founded Aya Data in early 2021 – recognizing the opportunity
           to help cultivate technology capability in the region.
            </p>
          </div>
        </div>
      </div>

      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img style="max-width:70%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" title="Jess"
       src="/img/jess.png" alt="" class="img-responsive">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Jesslin Anna Efua Hammond</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/jesslin-anna-efua-hammond-5b51b7128" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="position:relative; display:inline-block;top:-4px;margin-left:4px ">  Human Resource Manager</p>                   
            </div>
            <p class="team-text">Jesslin has spent the last 9 years managing corporate HR functions. She is particularly passionate about designing and implementing effective structured training programs,
               which is where she spends a lot of her time at Aya Data.
            </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  style="max-width:97%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" title="Edward"src="/img/eddie.png" alt="" class="img-responsive">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Edward Agyemang-Duah (M.Eng) </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/edward-agyemang-duah-6b4a6090/?originalSubdomain=gh" target="_bank"><span class="fa fa-linkedin"></span></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;">Team Lead</p>
            </div>
            <p class="team-text">Edward Agyemang-Duah is a Ph.D. fellow at the Zhejiang GongShang University, in the field of Statistics and Big Data, with a background in Machine Learning, specifically Recommender Systems. 
            Edward also has over 4 years experience in the Software development industry.
</p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img style="max-width:90%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" title="Stephen"src="/img/steeve.jpg" alt="" class="img-responsive">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Stephen Kwadwo Owusu Yiadom</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/stephen-owusu/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;">Technology Lead</p>
            </div>
            <p class="team-text">Stephen is a Computer Engineer graduate from Ashesi University with a background in Networking, development and Machine learning. 
    At Aya Data Stephen doubles as both a technology and team lead – running projects, and managing Aya’s technology partner integration.

            </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/Prince.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Prince <br> Oberko </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/prince-kwesi-oberko-3620073b/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="position:relative; display:inline-block;top:-28px">Sen. Data Protection Officer</p>
            </div>
            <p class="team-text">
            "Prince is a Software Engineering Ph.D. candidate at UEST, China, specializing in cryptography and network security. 
Prince has over six years of experience in Information Security and more than four years 
of experience in research into Information Security and Cybersecurity.
           </p>
          </div>
        </div>
      </div>

      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/selorm.png" alt="" class="img-responsive" style="border-radius:50%;">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Selorm  Kosi <br> Lodonu</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/selorm-lodonu-a00263148/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;">Data Protection Officer</p>
            </div>
            <p class="team-text">Selorm is a Computer Engineer graduate from Ashesi University with a background in Networking systems. 
              Here at Aya Data he serves as the Junior Data Protection Officer.
            </p>
          </div>
        </div>
      </div>

      
       <!--== Single Team Item ==-->
       <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/charlotte.png" alt="" class="img-responsive" style="border-radius:50%">
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Charlotte Maxine <br> Dawson-Amoah</h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/charlotte-dawson-amoah-2019/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;">Business Dev. Analyst</p>
            </div>
            <p class="team-text">Charlotte Dawson-Amoah is a recent Electrical and Electronic 
              Engineering graduate from Ashesi University with four years training and education in electrical and electronic systems, leadership and project management. 
         At Aya Data Charlotte focuses on customer outreach and acquisition.
          </p>
          </div>
        </div>
      </div>
<!--== Single Team Item ==-->
<!--<div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/prosper.png" alt="" class="img-responsive" style="border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Prosper <br> Kuuridong </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/prosper-kuuridong-44408b38/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title">Business Developer</p>
            </div>
            <p class="team-text"> 
            Prosper is an Information Systems graduate, from the Ghana Institute Of Management and Public Administration (GIMPA) 
            with Interest in Business Intelligence and a welcoming personality.
            Prosper has experience in business development and operations management.
           <br>  </p>
          </div>
        </div>
      </div>-->
<!--== Single Team Item ==-->
<div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/Eben.png" alt="" class="img-responsive"   style="width:100%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Ebenezer <br> Yeboah Yanney </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/ebenezer-yeboah-yanney-115789132/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;"> Web Developer </p>
            </div>
            <p class="team-text">Eben has spent 4 years as an account manager in the tech industry and a further 4 as a freelance web developer. 
Eben is a Business Admin graduate from Central University College and an engineering graduate from NIIT.
 </p>
            </div>
        </div>
      </div>


      <!--== Single Team Item ==-->
<div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/adnan.png" alt="" class="img-responsive"   style="max-width:98%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Adnan <br>  Inusah </h3>
              <div class="sci">
                    <a href="https://www.credly.com/users/adnan-inusah/badges" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Adnan is an Accounting and Finance graduate and a recent beneficiary of IBM Digital - Nation Africa's Data Science and Analytics course and IBM cognitive class with over 20 badges and credentials. 
He is an experienced AI Data Specialist skilled in Data annotation. 
    </p>
          </div>
        </div>
      </div>
     
       <!--== Single Team Item ==-->
<div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/amanda.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Amanda <br>  Appianteng </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/amanda-appiateng-656183216/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Amanda began her career working as an assistant to the head of Finance at COCOBOD, before her love of technology inspired her to move into AI. 
Here at Aya she is a Data Specialist - but doubles as a student of ICA (Institute of Chartered Accountants) Ghana.


            </p>
          </div>
        </div>
      </div>
       <!--== Single Team Item ==-->
<div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/sharon.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Sharon <br>  Chinyere Iphy </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/sharon-iphy-227b97195/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Sharon is a recent graduate of OpenLabs – NIIT, where she studied Software Engineering. Sharon represented her school in Ghana’s first ever virtual Hackathon, where they emerged 11th in Africa. 
Sharon is a trained AI Data Specialist skilled in Data Annotation, Data Processing and Quality Control.</p>

          </div>
        </div>
      </div>
     
  <!--== Single Team Item ==-->
  <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
           <img  src="/img/jeanne.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3> Jeanne<br>  A. Ocran </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/jeanne-ocran-887ab919a/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Jeanne Ocran is a Psychology and English graduate with a keen interest in AI and its applications in English Language. 
Here at Aya Data she serves as a data specialist with a particular focus on Natural Language Processing.
<p>
          </div>
        </div>
      </div>
  <!--== Single Team Item ==-->
  <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/Lee.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3> Lydia<br> Frimpong </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/lydia-frimpong-6a5b62136/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Lydia is a Social Work graduate from KNUST and has a collective five years of experience in areas of customer care and research. 
Lydia has worked with companies like AirtelTigo and Tonaton. Here at Aya data she serves as a data specialist and is adept at the annotation of ambiguous data.

            </p>
          </div>
        </div>
      </div>

      <!--== Single Team Item ==-->
    
  <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/Babs.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Barbara <br> Dankyi </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/barbara-dankyi-524205207/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">Barbara has over 6 years experience working as a Sales Promoter, Virtual Assistant and a Secretary, she is a graduate from UPSA with a degree in Marketing. 
            Here at Aya data she serves as a data specialist with a focus on Computer Vision.

            </p>
          </div>
        </div>
      </div>
     <!--== Single Team Item ==-->
     <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/Tim.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Timothy <br> C-Debrah </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/timothy-charles-debrah-65781914a/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">
            Timothy is a computer engineering graduate from Ashesi University who has gained skills in data entry,
             data management and in IoT systems. 
            He serves as a data specialist at Aya Data. 

            </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
     <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/vic.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Victoria <br> Akosua-Yirenkyi </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/akosua-yirenkyi-325069129/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">
            Victoria is a marketing graduate from the University of Professional Studues. She worked with the Cocoa Marketing
            Company as a warehouse and ports operations clerk. She serves as a data specialist at Aya Data and also doubles
            as a Law student.   
           </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
     <div class="col-md-3">
        <div class="single-team">
          <div class="team-img">
            <img  src="/img/SteveMens.png" alt="" class="img-responsive"   style="max-width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Stephen <br> Mensah </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/stephen-mensah-298266143/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">
            Stephen holds a Bachelor of Science in Information Technology from the University of Ghana. 
            He worked as the IT officer for the International Programs Office, University of Ghana. 
             He is a trained AI Data Specialist skilled in Data Annotation, Data Processing and Quality Control.
           </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
     <div class="col-md-3">
        <div class="single-team" style="height:94%;">
          <div class="team-img">
            <img  src="/img/faisal.png" alt="" class="img-responsive"   style="width:100% display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Faisal <br>Wahab </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/faisalwahabu/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title" style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">
            Faisal is a recent graduate from Ashesi University.
             He studied Electrical and Electronics engineering as a MasterCard Foundation Scholar. He is in love with computers, 
            Mathematics and artificial intelligence.He is a data specialist with Aya Data,
             ensuring high data quality in the Machine Learning Loop.
           </p>
          </div>
        </div>
      </div>
      <!--== Single Team Item ==-->
      <div class="col-md-3">
        <div class="single-team" style="height:94%">
          <div class="team-img">
            <img  src="/img/Nelly.png" alt="" class="img-responsive"   style="width:100%; display: block; 
      margin-left: auto; margin-right: auto; border-radius:50%" >
          </div>
          <div class="team-content">
            <div class="team-info">
              <h3>Felicia <br>Cokah </h3>
              <div class="sci">
                    <a href="https://www.linkedin.com/in/stephen-mensah-298266143/" target="_bank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
            </div>
              <p class="title"style="margin-left:28px; text-align:center;"> Data Specialist</p>
            </div>
            <p class="team-text">
            Felicia is a graduate of the University of Ghana with a Bachelor of Arts degree in Information Studies and Philosophy. After several years working as an Administrative Assistant, 
            she has transitioned to the role of Data Specialist at Aya Data in order to develop her technical skills.
           </p>
          </div>
        </div>
      </div>


    </div>
  </div>
</div>

    </div>
  </div>
</div>


          


</body>
</div>
</html>
<!--Leadership ends here-->
</x-app-layout>
